import { MenuComponent } from './pages/menu/menu.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { SesionComponent } from './pages/sesion/sesion.component';
import { HistoriaComponent } from './pages/historia/historia.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'historia', component: HistoriaComponent },
  { path: 'sesion', component: SesionComponent },
  { path: 'paciente', component: PacienteComponent },
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'menu', component: MenuComponent },
  {
    path: '',
    redirectTo: '/sesion',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
