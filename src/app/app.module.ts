import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { HistoriaComponent } from './pages/historia/historia.component';
import { SesionComponent } from './pages/sesion/sesion.component';
import { MenuComponent } from './pages/menu/menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './pages/shared/navbar/navbar.component';
import { FooterComponent } from './pages/shared/footer/footer.component';
import { HistoriaService } from './pages/historia/historia.service';
import { PacienteService } from './pages/paciente/paciente.service';
import { SesionService } from './pages/sesion/sesion.service';
import { UsuariosService } from './pages/usuarios/usuarios.service';
@NgModule({
  declarations: [
    AppComponent,
    UsuariosComponent,
    PacienteComponent,
    HistoriaComponent,
    SesionComponent,
    MenuComponent,
    NavbarComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    HistoriaService,
    PacienteService,
    SesionService,
    UsuariosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
