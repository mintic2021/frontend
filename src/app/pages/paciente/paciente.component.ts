import { PacienteService } from "./paciente.service";
import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import swal from "sweetalert2";
@Component({
  selector: "app-paciente",
  templateUrl: "./paciente.component.html",
  styleUrls: ["./paciente.component.scss"],
})
export class PacienteComponent implements OnInit {
  reactiveForm!: FormGroup;
  titularAlerta: string = "";
  viewTable: boolean = true;
  dataList: any[] = [];
  dataPaciente : any;
  statusButton : boolean = true;
  cedula: any;
  id : number = 0;
  
  constructor(
    private fb: FormBuilder,
    private pacienteService: PacienteService,
    private detectChange :  ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  setdata():void{
    this.dataPaciente = {...this.dataPaciente,
      nombre: this.reactiveForm.controls.nombre.value,
      cedula:this.reactiveForm.controls.cedula.value,
      nacimiento:this.reactiveForm.controls.nacimiento.value,
      celular:this.reactiveForm.controls.celular.value,
      sexo:this.reactiveForm.controls.sexo.value,
      edad:this.reactiveForm.controls.edad.value,
      eps:this.reactiveForm.controls.eps.value,
      otraeps:this.reactiveForm.controls.otraeps.value,
      medicinaPrepagada:this.reactiveForm.controls.medicinaPrepagada.value,
      otramed:this.reactiveForm.controls.otramed.value,
      estadocivil:this.reactiveForm.controls.estadocivil.value,
      hijos:this.reactiveForm.controls.hijos.value,
      cuantoshijos:this.reactiveForm.controls.cuantoshijos.value,
      direccion:this.reactiveForm.controls.direccion.value,
      ciudad:this.reactiveForm.controls.ciudad.value,
      otraciudad:this.reactiveForm.controls.otraciudad.value,
      nombreContacto:this.reactiveForm.controls.nombreContacto.value,
      parentesco:this.reactiveForm.controls.parentesco.value,
      celularContacto:this.reactiveForm.controls.celularContacto.value,
    }
  }

  guardar(): void {
    this.setdata();
    this.pacienteService
    .guardar(this.dataPaciente)
    .subscribe((response: any): void => {
      if (response) {
        swal.fire("Registro exitoso...", this.titularAlerta, "success");
        this.reactiveForm.reset();
        this.statusButton = true;
      } else {
        swal.fire(
          "Error al guardar el registro...",
          this.titularAlerta,
          "error"
        );
      }
    });
  }

  actualizar(): void {
    this.setdata();
      this.pacienteService
      .actualizar(this.dataPaciente, this.id)
      .subscribe((response: any): void => {
        if (response) {
          swal.fire("Registro actualizado...", this.titularAlerta, "success");
          this.reactiveForm.reset();
          this.statusButton = true;
        } else {
          swal.fire(
            "Ops no fué posible al actualizar el registro...",
            this.titularAlerta,
            "error"
          );
        }
      });
  }

  eliminar(): void {
    this.pacienteService.eliminar(this.id).subscribe((response: any): void => {
      if (response) {
        swal.fire("Registro eliminado...", this.titularAlerta, "success");
        this.reactiveForm.reset();
        this.statusButton = true;
      } else {
        swal.fire(
          "Ops no fué posible eliminar el registro...",
          this.titularAlerta,
          "error"
        );
      }
    });
  }

  buscar(): void {
    this.setdata();
    const id: number = this.dataPaciente.cedula;
    this.pacienteService.buscar(id).subscribe((response: any): void => {
      if (response.length > 0) {
        this.cedula = response[0].cedula;
        this.id = response[0].id;
        this.statusButton = true;
        swal.fire("Registro encontrado...", this.titularAlerta, "success");
        this.reactiveForm.controls.nombre.setValue(response[0].nombre);
        this.reactiveForm.controls.nacimiento.setValue(response[0].nacimiento);
        this.reactiveForm.controls.celular.setValue(response[0].celular);
        this.reactiveForm.controls.sexo.setValue(response[0].sexo);
        this.reactiveForm.controls.edad.setValue(response[0].edad);
        this.reactiveForm.controls.eps.setValue(response[0].eps);
        this.reactiveForm.controls.otraeps.setValue(response[0].otraeps);
        this.reactiveForm.controls.medicinaPrepagada.setValue(response[0].medicinaPrepagada);
        this.reactiveForm.controls.otramed.setValue(response[0].otramed);
        this.reactiveForm.controls.estadocivil.setValue(response[0].estadocivil);
        this.reactiveForm.controls.hijos.setValue(response[0].hijos);
        this.reactiveForm.controls.cuantoshijos.setValue(response[0].cuantoshijos);       
        this.reactiveForm.controls.direccion.setValue(response[0].direccion);
        this.reactiveForm.controls.ciudad.setValue(response[0].ciudad);
        this.reactiveForm.controls.otraciudad.setValue(response[0].otraciudad);        
        this.reactiveForm.controls.nombreContacto.setValue(response[0].nombreContacto);
        this.reactiveForm.controls.parentesco.setValue(response[0].parentesco);
        this.reactiveForm.controls.celularContacto.setValue(response[0].celularContacto);
        } else {
        swal.fire(
          "Ops no se encontró un registro para este paciente...",
          this.titularAlerta,
          "error"
        );
      }
    });
  }

  buildForm(): void {
    this.reactiveForm = this.fb.group({
      nombre: new FormControl("", [Validators.required]),
      cedula: new FormControl("", [Validators.required]),
      nacimiento: new FormControl("", [Validators.required]),
      celular: new FormControl("", [Validators.required]),
      sexo: new FormControl("", [Validators.required]),
      edad: new FormControl("", [Validators.required]),
      eps: new FormControl("", [Validators.required]),
      otraeps: new FormControl("" ),
      medicinaPrepagada: new FormControl("", [Validators.required]),
      otramed:new FormControl("" ),
      estadocivil: new FormControl("", [Validators.required]),
      hijos: new FormControl("", [Validators.required]),
      cuantoshijos: new FormControl(""),
      direccion: new FormControl(""),
      ciudad: new FormControl(""),
      otraciudad: new FormControl(""),
      nombreContacto: new FormControl("", [Validators.required]),
      parentesco: new FormControl(""),
      celularContacto: new FormControl("", [Validators.required]),
    });

    this.reactiveForm.valueChanges.subscribe((value) => {

    });
  }
}
