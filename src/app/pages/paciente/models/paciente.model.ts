export namespace PacienteModel { 



    export interface IPacienteHttp { 
          nombre: string,
          celular: string,
          edad: number,
          nacimiento: string,
          eps: string,
          cedula: number,
          sexo: string,
          otraeps: string,
          medicinaPrepagada: string,
          otramed: string,
          estadocivil: string,
          hijos: number,
          cuantoshijos: number,
          direccion: string,
          ciudad: string,
          otraciudad: string,
          nombreContacto: string,
          parentesco: string,
          celularContacto: string,
          }
          
         
    export const DEFAULT_DATA_PACIENTE : IPacienteHttp = {
        nombre: '',
        celular: '',
        edad: 0,
        nacimiento: '',
        eps: 'No aplica',
        cedula: 0,
        sexo: '',
        otraeps: " ",
        medicinaPrepagada: " ",
        otramed: " ",
        estadocivil: " ",
        hijos: 0,
        cuantoshijos: 0,
        direccion: " ",
        ciudad: " ",
        otraciudad: " ",
        nombreContacto: " ",
        parentesco: " ",
        celularContacto: " ",
        }
    
    }