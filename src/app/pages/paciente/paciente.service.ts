import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, catchError } from "rxjs/operators";
import { throwError } from "rxjs";
import { PacienteModel } from "./models/paciente.model";

@Injectable({
  providedIn: "root",
})
export class PacienteService {
  router: string = "http://localhost:8080/paciente";

  constructor(private http: HttpClient) {}

  guardar(data: PacienteModel.IPacienteHttp) {
    const dataForm = data;

    console.log(dataForm);

    let response: any;
    return this.http
      .post(this.router, {
        nombre: dataForm.nombre,
        cedula: dataForm.cedula,
        nacimiento: dataForm.nacimiento,
        celular: dataForm.celular,
        sexo: dataForm.sexo,
        edad: dataForm.edad,
        eps: dataForm.eps,
        otraeps: dataForm.otraeps,
        medicinaPrepagada: dataForm.medicinaPrepagada,
        otramed: dataForm.otramed,
        estadocivil: dataForm.estadocivil,
        hijos: dataForm.hijos,
        cuantoshijos: dataForm.cuantoshijos,
        direccion: dataForm.direccion,
        ciudad: dataForm.ciudad,
        otraciudad: dataForm.otraciudad,
        nombreContacto: dataForm.nombreContacto,
        parentesco: dataForm.parentesco,
        celularContacto: dataForm.celularContacto,
      })
      .pipe(
        map((request: any): any => {
          response = request;
          return response;
        }),

        catchError((err) => {
          console.log("error caught in service");
          console.error(err);
          return throwError(err);
        })
      );
  }

  buscar(id: number) {
    let response: any;
    return this.http.get(this.router + "/" + id.toString()).pipe(
      map((request: any): any => {
        response = request;
        return response;
      }),

      catchError((err) => {
        console.log("error caught in service");
        console.error(err);
        return throwError(err);
      })
    );
  }

  actualizar(data: PacienteModel.IPacienteHttp, id: number) {
    const dataForm = data;

    let response: any;
    return this.http
      .post(this.router, {
        id: id,
        cedula: dataForm.cedula,
        nombre: dataForm.nombre,
        nacimiento: dataForm.nacimiento,
        celular: dataForm.celular,
        sexo: dataForm.sexo,
        edad: dataForm.edad,
        eps: dataForm.eps,
        otraeps: dataForm.otraeps,
        medicinaPrepagada: dataForm.medicinaPrepagada,
        otramed: dataForm.otramed,
        estadocivil: dataForm.estadocivil,
        hijos: dataForm.hijos,
        cuantoshijos: dataForm.cuantoshijos,
        direccion: dataForm.direccion,
        ciudad: dataForm.ciudad,
        otraciudad: dataForm.otraciudad,
        nombreContacto: dataForm.nombreContacto,
        parentesco: dataForm.parentesco,
        celularContacto: dataForm.celularContacto,
      })
      .pipe(
        map((request: any): any => {
          response = request;
          return response;
        }),

        catchError((err) => {
          console.log("error caught in service");
          console.error(err);
          return throwError(err);
        })
      );
  }

  eliminar(id: number) {
    let response: any;
    return this.http.delete(this.router + "/" + id.toString()).pipe(
      map((request: any): any => {
        response = request;
        return response;
      }),

      catchError((err) => {
        console.log("error caught in service");
        console.error(err);
        return throwError(err);
      })
    );
  }
}
