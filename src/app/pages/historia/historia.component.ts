import { HistoriaService } from "./historia.service";
import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import swal from "sweetalert2";
import { HistoriaClass } from "./class/historia.class";
import { HistoriaModel } from "./models/historia.model";

@Component({
  selector: "app-historia",
  templateUrl: "./historia.component.html",
  styleUrls: ["./historia.component.scss"],
})
export class HistoriaComponent implements OnInit {
  reactiveForm!: FormGroup;
  titularAlerta: string = "";
  viewTable: boolean = true;
  dataList: any[] = [];
  class = new HistoriaClass();
  dataHistory: HistoriaModel.IHistoriaHttp =
    HistoriaModel.DEFAULT_DATA_HISTORIA;
  statusButton: boolean = true;
  idRegistro: number = 0;
  statusButtonSave: boolean = true;

  constructor(
    private fb: FormBuilder,
    private historiaService: HistoriaService,
    private detectChange: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  guardar(): void {
    this.historiaService
      .guardar(this.dataHistory)
      .subscribe((response: any): void => {
        if (response) {
          swal.fire("Registro exitoso...", this.titularAlerta, "success");
          this.reactiveForm.reset();
          this.statusButton = true;
        } else {
          swal.fire(
            "Ops no fué posible guardar el registro...",
            this.titularAlerta,
            "error"
          );
        }
      });
  }

  actualizar(): void {
    this.historiaService
      .actualizar(this.dataHistory, this.idRegistro)
      .subscribe((response: any): void => {
        if (response) {
          swal.fire("Registro actualizado...", this.titularAlerta, "success");
          this.reactiveForm.reset();
          this.statusButton = true;
        } else {
          swal.fire(
            "Ops no fué posible al actualizar el registro...",
            this.titularAlerta,
            "error"
          );
        }
      });
  }

  eliminar(): void {
    this.historiaService
      .eliminar(this.idRegistro)
      .subscribe((response: any): void => {
        if (response) {
          swal.fire("Registro eliminado...", this.titularAlerta, "success");
          this.reactiveForm.reset();
          this.statusButton = true;
        } else {
          swal.fire(
            "Ops no fué posible eliminar el registro...",
            this.titularAlerta,
            "error"
          );
        }
      });
  }

  buscar(): void {
    const id: number = this.dataHistory.identification;

    this.historiaService.buscar(id).subscribe((response: any): void => {
      if (response.length > 0) {
        this.idRegistro = response[0].id;
        this.statusButtonSave = true;
        swal.fire("Registro encontrado...", this.titularAlerta, "success");
        this.reactiveForm.controls.name.setValue(response[0].name);
        this.reactiveForm.controls.birthday.setValue(response[0].birthDay);
        this.reactiveForm.controls.phone.setValue(response[0].phone);
        this.reactiveForm.controls.sex.setValue(response[0].sex);
        this.reactiveForm.controls.age.setValue(response[0].age);
        this.reactiveForm.controls.eps.setValue(response[0].eps);
        this.reactiveForm.controls.medicalPre.setValue(response[0].medical);
        this.reactiveForm.controls.dateQuery.setValue(response[0].dateMedical);
        this.reactiveForm.controls.dateTime.setValue(response[0].timerMedical);
        this.reactiveForm.controls.weight.setValue(response[0].weight);
        this.reactiveForm.controls.height.setValue(response[0].height);
        this.reactiveForm.controls.imc.setValue(response[0].imc);

        this.reactiveForm.controls.alergias.setValue({
          flexRadioDefault11: response[0].statusAlergia,
          which: response[0].alergia,
        });

        this.reactiveForm.controls.antecedentes.setValue({
          flexRadioDefault22: response[0].statusAntecedente,
          which: response[0].antecendente,
        });

        this.reactiveForm.controls.fumar.setValue({
          flexRadioDefault33: response[0].statusFuma,
          which: response[0].timesFuma,
        });

        this.reactiveForm.controls.ejercicio.setValue({
          flexRadioDefault44: response[0].statusExercise,
          which: response[0].timesExercise,
        });

        this.reactiveForm.controls.cirugias.setValue({
          flexRadioDefault55: response[0].statusCirugia,
          which: response[0].cirugias,
        });

        this.reactiveForm.controls.hospitalizacion.setValue({
          flexRadioDefault: response[0].statusHospital,
        });

        this.reactiveForm.controls.motivo.setValue(response[0].motivo);
        this.reactiveForm.controls.observation.setValue(
          response[0].observations
        );
        this.reactiveForm.controls.exams.setValue(response[0].examenes);
        this.reactiveForm.controls.formula.setValue(response[0].formula);
      } else {
        swal.fire(
          "Ops no se encontró un registro para este paciente...",
          this.titularAlerta,
          "error"
        );
      }
    });
  }

  volver(): void {
    this.viewTable = !this.viewTable;
  }

  listar(): void {
    this.historiaService.listar().subscribe((response: any[]): void => {
      if (response) {
        this.dataList = response;
        this.viewTable = false;
        swal.fire("Registros encontrados...", this.titularAlerta, "success");
      } else {
        swal.fire(
          "Error al buscar el registro...",
          this.titularAlerta,
          "error"
        );
      }
    });
  }

  buildForm(): void {
    this.reactiveForm = this.fb.group({
      name: new FormControl("", [Validators.required]),
      identification: new FormControl([Validators.required]),
      birthday: new FormControl("", [Validators.required]),
      phone: new FormControl([Validators.required]),
      sex: new FormControl("", [Validators.required]),
      age: new FormControl([Validators.required]),
      eps: new FormControl("", [Validators.required]),
      medicalPre: new FormControl("", [Validators.required]),
      dateQuery: new FormControl("", [Validators.required]),
      dateTime: new FormControl("9:44 PM", [Validators.required]),
      weight: new FormControl([Validators.required]),
      height: new FormControl([Validators.required]),
      imc: new FormControl("", [Validators.required]),

      alergias: this.fb.group({
        flexRadioDefault11: new FormControl(""),
        which: new FormControl(""),
      }),

      antecedentes: this.fb.group({
        flexRadioDefault22: new FormControl(""),
        which: new FormControl(""),
      }),

      fumar: this.fb.group({
        flexRadioDefault33: new FormControl(""),
        which: new FormControl(""),
      }),

      ejercicio: this.fb.group({
        flexRadioDefault44: new FormControl(""),
        which: new FormControl(""),
      }),

      cirugias: this.fb.group({
        flexRadioDefault55: new FormControl(""),
        which: new FormControl(""),
      }),

      hospitalizacion: this.fb.group({
        flexRadioDefault: new FormControl(""),
      }),

      motivo: new FormControl("", [Validators.required]),
      observation: new FormControl("", [Validators.required]),
      exams: new FormControl("", [Validators.required]),
      formula: new FormControl("", [Validators.required]),
    });

    this.reactiveForm.valueChanges.subscribe((value) => {
      this.dataHistory = this.class.convertData(value);
      this.statusButton = !this.reactiveForm.valid;
      this.statusButtonSave =
        !this.reactiveForm.valid && this.idRegistro == 0 ? false : true;
      this.detectChange.detectChanges();
    });
  }
}
