import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, catchError } from "rxjs/operators";
import { throwError } from "rxjs";
import { HistoriaModel } from "./models/historia.model";

@Injectable({
  providedIn: "root",
})
export class HistoriaService {
  router: string = "http://localhost:8080/history";

  constructor(private http: HttpClient) {}

  guardar(data: HistoriaModel.IHistoriaHttp) {
    const dataForm = data;

    let response: any;
    return this.http
      .post(this.router, {
        name: dataForm.name,
        phone: dataForm.phone,
        age: dataForm.age,
        alergia: dataForm.alergia,
        antecendente: dataForm.antecedentes,
        birthDay: dataForm.birth_day,
        cirugias: dataForm.cirugias,
        dateMedical: dataForm.date_medical,
        eps: dataForm.eps,
        examenes: dataForm.examenes,
        formula: dataForm.formula,
        height: dataForm.height,
        identification: dataForm.identification,
        imc: dataForm.imc,
        medical: dataForm.medical,
        motivo: dataForm.motivo,
        observations: dataForm.observations,
        sex: dataForm.sex,
        statusAlergia: dataForm.status_alergia,
        statusAntecedente: dataForm.status_antecedente,
        statusCirugia: dataForm.status_cirugia,
        statusExercise: dataForm.status_exercise,
        statusFuma: dataForm.status_fuma,
        statusHospital: dataForm.status_hospital,
        timerMedical: dataForm.timer_medical,
        timesExercise: dataForm.times_exercise,
        timesFuma: dataForm.times_fuma,
        weight: dataForm.weight,
      })
      .pipe(
        map((request: any): any => {
          response = request;
          return response;
        }),

        catchError((err) => {
          console.log("error caught in service");
          console.error(err);
          return throwError(err);
        })
      );
  }

  buscar(id: number) {
    let response: any;
    return this.http.get(this.router + "/" + id.toString()).pipe(
      map((request: any): any => {
        response = request;
        return response;
      }),

      catchError((err) => {
        console.log("error caught in service");
        console.error(err);
        return throwError(err);
      })
    );
  }

  actualizar(data: HistoriaModel.IHistoriaHttp, id: number) {
    const dataForm = data;

    let response: any;
    return this.http
      .post(this.router, {
        id: id,
        name: dataForm.name,
        phone: dataForm.phone,
        age: dataForm.age,
        alergia: dataForm.alergia,
        antecendente: dataForm.antecedentes,
        birthDay: dataForm.birth_day,
        cirugias: dataForm.cirugias,
        dateMedical: dataForm.date_medical,
        eps: dataForm.eps,
        examenes: dataForm.examenes,
        formula: dataForm.formula,
        height: dataForm.height,
        identification: dataForm.identification,
        imc: dataForm.imc,
        medical: dataForm.medical,
        motivo: dataForm.motivo,
        observations: dataForm.observations,
        sex: dataForm.sex,
        statusAlergia: dataForm.status_alergia,
        statusAntecedente: dataForm.status_antecedente,
        statusCirugia: dataForm.status_cirugia,
        statusExercise: dataForm.status_exercise,
        statusFuma: dataForm.status_fuma,
        statusHospital: dataForm.status_hospital,
        timerMedical: dataForm.timer_medical,
        timesExercise: dataForm.times_exercise,
        timesFuma: dataForm.times_fuma,
        weight: dataForm.weight,
      })
      .pipe(
        map((request: any): any => {
          response = request;
          return response;
        }),

        catchError((err) => {
          console.log("error caught in service");
          console.error(err);
          return throwError(err);
        })
      );
  }

  eliminar(id: number) {
    let response: any;
    return this.http.delete(this.router + "/" + id.toString()).pipe(
      map((request: any): any => {
        response = request;
        return response;
      }),

      catchError((err) => {
        console.log("error caught in service");
        console.error(err);
        return throwError(err);
      })
    );
  }

  listar() {
    let response: any[];
    return this.http.get(this.router).pipe(
      map((request: any): any => {
        response = request;
        return response;
      }),

      catchError((err) => {
        console.log("error caught in service");
        console.error(err);
        return throwError(err);
      })
    );
  }
}
