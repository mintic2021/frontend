import { HistoriaModel } from "../models/historia.model";

export class HistoriaClass {
  public data: HistoriaModel.IHistoriaHttp =
    HistoriaModel.DEFAULT_DATA_HISTORIA;

  convertData(data: any): HistoriaModel.IHistoriaHttp {
    const dataForm= data;

    this.data = {
      ...this.data,

      name: dataForm.name,
      phone: dataForm.phone,
      age: dataForm.age,
      alergia: dataForm.alergias.which,
      antecedentes: dataForm.antecedentes.which,
      birth_day: dataForm.birthday,
      cirugias: dataForm.cirugias.which,
      date_medical: dataForm.dateQuery,
      eps: dataForm.eps,
      examenes: dataForm.exams,
      formula: dataForm.formula,
      height: dataForm.height,
      identification: dataForm.identification,
      imc: dataForm.imc,
      medical: dataForm.medicalPre,
      motivo: dataForm.motivo,
      observations: dataForm.observation,
      sex: dataForm.sex,
      status_alergia: dataForm.alergias.flexRadioDefault11,
      status_antecedente: dataForm.antecedentes.flexRadioDefault22,
      status_cirugia: dataForm.cirugias.flexRadioDefault55,
      status_exercise: dataForm.ejercicio.flexRadioDefault44,
      status_fuma: dataForm.fumar.flexRadioDefault33,
      status_hospital: dataForm.hospitalizacion.flexRadioDefault,
      timer_medical: dataForm.dateTime,
      times_exercise: dataForm.ejercicio.which,
      times_fuma: dataForm.fumar.which,
      weight: dataForm.weight,
    };

    return this.data;
  }
}
