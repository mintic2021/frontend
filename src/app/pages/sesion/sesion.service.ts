import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, catchError } from "rxjs/operators";
import { throwError } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SesionService {
  router: string = "http://localhost:8080/usuarios"; //ENDPOINT

  constructor(private http: HttpClient) {}

  autenticar(email: string, contrasena: string) {
    let response: any;
    return this.http.get(this.router + "/" + email).pipe(
      map((request: any): any => {
        response = request;
        return response;
      }),

      catchError((err) => {
        console.log("error caught in service");
        console.error(err);
        return throwError(err);
      })
    );
  }
}
