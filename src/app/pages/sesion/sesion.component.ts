import { SesionService } from "./sesion.service";
import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import swal from "sweetalert2";
import { Router } from "@angular/router";
@Component({
  selector: "app-sesion",
  templateUrl: "./sesion.component.html",
  styleUrls: ["./sesion.component.scss"],
})
export class SesionComponent implements OnInit {
  reactiveForm!: FormGroup;
  titularAlerta: string = "";
  constructor(
    private fb: FormBuilder,
    private sesionService: SesionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.reactiveForm = this.fb.group({
      usuario: new FormControl("", [Validators.required]),
      contrasena: new FormControl("", [Validators.required]),
    });

    this.reactiveForm.valueChanges.subscribe((value) => {});
  }

  iniciarSesion(): void {
    const correo: string = this.reactiveForm.controls.usuario.value;
    const contrasena: string = this.reactiveForm.controls.contrasena.value;

    this.sesionService
      .autenticar(correo, contrasena)
      .subscribe((response: any): void => {
        if (
          response[0]?.password == this.reactiveForm.controls.contrasena.value
        ) {
          swal.fire(
            "Sesión iniciada correctamente...",
            this.titularAlerta,
            "success"
          );
          this.router.navigateByUrl("/menu");
        } else {
          swal.fire(
            "Usuario o contraseña no válidos...",
            this.titularAlerta,
            "error"
          );
        }
      });
  }
}
