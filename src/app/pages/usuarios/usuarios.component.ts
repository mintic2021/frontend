import { UsuariosService } from "./usuarios.service";
import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import swal from "sweetalert2";

@Component({
  selector: "app-usuarios",
  templateUrl: "./usuarios.component.html",
  styleUrls: ["./usuarios.component.scss"],
})
export class UsuariosComponent implements OnInit {
  reactiveForm!: FormGroup;
  estado: boolean = false;
  statusBoton: boolean = true;
  titularAlerta: string = "";
  idUsuario : number = 0;
  viewTable: boolean = true;
  dataList: any[] = [];

  constructor(
    private fb: FormBuilder,
    private usuariosService: UsuariosService
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.reactiveForm = this.fb.group({
      name: new FormControl("", [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50),
      ]),
      email: new FormControl("", [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(40),
        Validators.email,
      ]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(10),
      ]),
      number: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
      ]),
    });

    this.reactiveForm.valueChanges.subscribe((value) => {      

      if (this.reactiveForm.valid) {
        this.statusBoton = false;
      } else {
        this.statusBoton = true;
      }
    });
  }

  guardar(): void {
    const dataForm: any = this.reactiveForm.value;
    
    this.usuariosService.guardar(dataForm).subscribe((response: any): void => {
      if (response) {
        swal.fire("Registro exitoso...", this.titularAlerta, "success");
        this.reactiveForm.reset();
      } else {
        swal.fire(
          "Error al guardar el registro...",
          this.titularAlerta,
          "error"
        );
      }
    });
  }

  actualizar(): void {
    const dataForm: any = this.reactiveForm.value;
    const idUsuario = this.idUsuario;

    this.usuariosService
      .actualizar(dataForm, idUsuario)
      .subscribe((response: any): void => {
        if (response) {
          swal.fire("Registro actualizado...", this.titularAlerta, "success");
          this.reactiveForm.reset();
        } else {
          swal.fire(
            "Error al actualizar el registro...",
            this.titularAlerta,
            "error"
          );
        }
      });
  }

  eliminar(): void {
    const id: any = this.idUsuario;

    this.usuariosService.eliminar(id).subscribe((response: any): void => {
      if (response) {
        swal.fire("Registro eliminado...", this.titularAlerta, "success");
        this.reactiveForm.reset();
      } else {
        swal.fire(
          "Error al eliminar el registro...",
          this.titularAlerta,
          "error"
        );
      }
    });
  }

  buscar(): void {
    const email: string = this.reactiveForm.controls.email.value;

    this.usuariosService.buscar(email).subscribe((response: any): void => {
      if (response.length > 0) {
        swal.fire("Registro encontrado...", this.titularAlerta, "success");
        this.idUsuario = response[0].id;
        this.reactiveForm.controls.name.setValue(response[0].name);
        this.reactiveForm.controls.email.setValue(response[0].email);
        this.reactiveForm.controls.number.setValue(response[0].number);
        this.reactiveForm.controls.password.setValue(response[0].password);
      } else {
        swal.fire(
          "Ops no encontramos el registro solicitado...",
          this.titularAlerta,
          "error"
        );
      }
    });
  }

  listar(): void {
    this.usuariosService.listar().subscribe((response: any[]): void => {
      if (response) {
        this.dataList = response;
        this.viewTable = false;
        swal.fire("Registros encontrados...", this.titularAlerta, "success");
      } else {
        swal.fire(
          "Error al buscar el registro...",
          this.titularAlerta,
          "error"
        );
      }
    });
  }

  volver(): void {
    this.viewTable = !this.viewTable;
  }
}
