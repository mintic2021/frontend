import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, catchError } from "rxjs/operators";
import { throwError } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class UsuariosService {
  router: string = "http://localhost:8080/usuarios"; //ENDPOINT

  constructor(private http: HttpClient) {}

  guardar(data: any) {    
    const dataForm = data;
    let response: any;

    return this.http
      .post(this.router, {
        name: dataForm.name,
        email: dataForm.email,
        password: dataForm.password,
        number: dataForm.number,
      })
      .pipe(
        map((request: any): any => {
          response = request;
          return response;
        }),

        catchError((err) => {
          console.log("error caught in service");
          console.error(err);
          return throwError(err);
        })
      );
  }

  buscar(email: string) {
    let response: any;
    return this.http.get(this.router + "/" + email).pipe(
      map((request: any): any => {
        response = request;
        return response;
      }),

      catchError((err) => {
        console.log("error caught in service");
        console.error(err);
        return throwError(err);
      })
    );
  }

  actualizar(data: any, id : number) {    
    const dataForm = data;
    let response: any;

    return this.http
      .post(this.router, {
        id : id,
        name: dataForm.name,
        email: dataForm.email,
        password: dataForm.password,
        number: dataForm.number,
      })
      .pipe(
        map((request: any): any => {
          response = request;
          return response;
        }),

        catchError((err) => {
          console.log("error caught in service");
          console.error(err);
          return throwError(err);
        })
      );
  }

  eliminar(id: number) {
    let response: any;
    return this.http.delete(this.router + "/" + id.toString()).pipe(
      map((request: any): any => {
        response = request;
        return response;
      }),

      catchError((err) => {
        console.log("error caught in service");
        console.error(err);
        return throwError(err);
      })
    );
  }

  listar() {
    let response: any[];
    return this.http.get(this.router).pipe(
      map((request: any): any => {
        response = request;
        return response;
      }),

      catchError((err) => {
        console.log("error caught in service");
        console.error(err);
        return throwError(err);
      })
    );
  }
}
